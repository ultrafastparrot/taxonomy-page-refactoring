import React from 'react';
import { LoadingOutlined, SearchOutlined } from '@ant-design/icons';
import { Input, InputProps } from 'antd';
import { debounce, DebounceSettings as LodashDebounceSettings } from 'lodash';

type DebounceSettings =
  | false
  | {
      delay: number;
      debounceSettings?: LodashDebounceSettings;
    };

export interface DebouncedSearchProps
  extends Omit<InputProps, 'onChange' | 'onPressEnter' | 'children'> {
  onSearch: (result: string) => void;
  loading?: boolean;
  debounced: DebounceSettings;
}

const InputSearch = React.forwardRef<Input, DebouncedSearchProps>(
  ({ onSearch, debounced, loading, ...rest }, ref) => {
    const inputRef = React.useRef<Input>(
      ref === null ? null : (ref as React.MutableRefObject<Input>).current
    );

    const lastSearchedTerm = React.useRef('');

    const handleSearch = (searchTerm: string) => {
      if (searchTerm === lastSearchedTerm.current) {
        return;
      }
      lastSearchedTerm.current = searchTerm;
      onSearch(searchTerm);
    };

    const debouncedOnSearch =
      debounced === false
        ? onSearch
        : debounce(handleSearch, debounced.delay, debounced.debounceSettings);

    const onPressEnter = () => {
      const searchTerm = inputRef.current?.input.value ?? '';
      handleSearch(searchTerm);
    };

    return (
      <Input
        type="search"
        placeholder="Pesquisar"
        suffix={loading ? <LoadingOutlined /> : <SearchOutlined />}
        onChange={(event) => debouncedOnSearch(event.target.value)}
        onPressEnter={onPressEnter}
        ref={inputRef}
        role="search"
        {...rest}
      />
    );
  }
);

export default InputSearch;
