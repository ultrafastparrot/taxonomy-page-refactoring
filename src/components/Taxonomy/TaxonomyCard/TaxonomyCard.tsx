import React from 'react';
import cx from 'classnames/dedupe';
import { PlusCircleIcon } from '@heroicons/react/outline';
import { Button } from 'antd';
import InputSearch from '../../InputSearch';
import TaxonomyItem from './TaxonomyItem';
import { getMenuActions } from './helpers';

export const TaxonomyTypes = ['BU', 'SEGMENT', 'OTHER'] as const;

export type TaxonomyType = typeof TaxonomyTypes[number];

export interface TaxonomyCardProps {
  className?: string;
  onClose?: () => void;
  taxonomyType: TaxonomyType;
}

const TaxonomyCard = ({
  className,
  onClose,
  taxonomyType,
}: TaxonomyCardProps): JSX.Element => {
  const [loading, setLoading] = React.useState(false);
  const [selectedItem, setSelectedItem] = React.useState<number>();

  const headerProps = cx(
    'relative flex bg-black text-white py-2 justify-center items-center',
    {
      'bg-black': taxonomyType === 'BU',
      'bg-green': taxonomyType === 'SEGMENT',
      'bg-gray-500': taxonomyType === 'OTHER',
    }
  );

  const handleSearch = () => {
    setLoading(true);

    setTimeout(() => {
      setLoading(false);
    }, 400);
  };

  const menuActions = getMenuActions(taxonomyType);

  return (
    <div
      className={`${cx(
        className,
        'flex flex-col items-stretch shadow-lg w-1/6 rounded'
      )}`}
    >
      <header className={headerProps}>
        <span className="self-center">T X</span>
        <button
          type="button"
          className="absolute right-2.5 transform hover:scale-110 transition duration-200"
          onClick={onClose}
        >
          <PlusCircleIcon className="h-5 w-5" />
        </button>
      </header>
      <div className="p-1">
        <InputSearch
          debounced={{ delay: 400 }}
          onSearch={handleSearch}
          loading={loading}
        />
      </div>
      <div className="h-full overflow-y-auto">
        <div>
          {[...Array(4).fill(0).keys()].map((index) => (
            <TaxonomyItem
              key={index}
              title="BU / Segmento"
              subtitle="Alimentar / B2B"
              actions={menuActions}
              count={index}
              selected={index === selectedItem}
              onSelected={() => setSelectedItem(index)}
              parentTaxonomyType={taxonomyType}
            />
          ))}
        </div>
      </div>
      <div className="flex-1" />
      <div className="bg-gray-100 px-4 py-2 border-t">
        <Button
          className="w-full border-0 h-8 bg-transparent text-blue-500 hover:text-blue-500 ring-1 ring-blue-500"
          type="ghost"
        >
          Carregar mais...
        </Button>
      </div>
    </div>
  );
};

export default TaxonomyCard;
