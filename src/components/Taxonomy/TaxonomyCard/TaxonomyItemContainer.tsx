import React, { HTMLProps } from 'react';
import cx from 'classnames/dedupe';

interface TaxonomyItemContainerProps extends HTMLProps<HTMLDivElement> {
  onKeyUp: (ev: React.KeyboardEvent<HTMLDivElement>) => void;
  onMouseUp: (ev: React.MouseEvent<HTMLDivElement>) => void;
  parentTaxonomyType: string;
  selected: boolean;
  onClick: () => void;
  children: React.ReactNode;
}

export const TaxonomyItemContainer = React.forwardRef<
  HTMLDivElement,
  TaxonomyItemContainerProps
>(
  (
    {
      onClick,
      onKeyUp,
      onMouseUp,
      parentTaxonomyType,
      selected,
      children,
      ...rest
    }: TaxonomyItemContainerProps,
    ref
  ): JSX.Element => (
    <div
      {...rest}
      ref={ref}
      onKeyUp={onKeyUp}
      onMouseUp={onMouseUp}
      role="radio"
      className={cx(
        'bg-gray-50 px-2 border-b py-2 border-gray-200 cursor-pointer hover:bg-gray-200 text-gray-500 uppercase',
        {
          'bg-green hover:bg-green': parentTaxonomyType === 'BU' && selected,
          'bg-gray-500 hover:bg-gray-500':
            parentTaxonomyType !== 'BU' && selected,
        },
        {
          'text-white': selected,
          'text-gray-500': !selected,
        }
      )}
      onClick={onClick}
      aria-checked={selected}
      tabIndex={0}
    >
      {children}
    </div>
  )
);
