import React from 'react';

import cx from 'classnames/dedupe';
import { InboxOutlined, RightOutlined } from '@ant-design/icons';
import { Dropdown, Menu } from 'antd';
import { TaxonomyType } from '.';
import { TaxonomyItemContainer } from './TaxonomyItemContainer';

export interface Action {
  name: string;
  icon: React.ReactNode;
  onClick: () => void;
}

export interface TaxonomyItemProps {
  title: string;
  subtitle: string;
  count: number;
  actions: Action[];
  selected: boolean;
  onSelected: () => void;
  parentTaxonomyType: TaxonomyType;
}

const RIGHT_BUTTON = 2;

const TaxonomyItem = ({
  title,
  subtitle,
  count,
  actions,
  selected,
  onSelected,
  parentTaxonomyType,
}: TaxonomyItemProps): JSX.Element => {
  const [dropdownVisible, setDropdownVisible] = React.useState(false);

  const handleKeyUp = (ev: React.KeyboardEvent<HTMLDivElement>) => {
    if (ev.key === 'Escape') {
      setDropdownVisible(false);
    }
  };

  const handleMouseUp = (ev: React.MouseEvent<HTMLDivElement>) => {
    if (ev.button === RIGHT_BUTTON) {
      setDropdownVisible(true);
    }
  };

  const handleActionClick = (action: Action) => {
    setDropdownVisible(false);
    action.onClick();
  };

  return (
    <Dropdown
      visible={dropdownVisible}
      onVisibleChange={setDropdownVisible}
      overlay={
        <Menu className="py-0">
          {actions.map((action) => (
            <Menu.Item
              key={action.name}
              className="text-gray-500"
              onClick={() => handleActionClick(action)}
            >
              <div className="flex gap-x-3 items-center">
                {action.icon}
                {action.name}
              </div>
            </Menu.Item>
          ))}
        </Menu>
      }
      trigger={['contextMenu']}
    >
      <TaxonomyItemContainer
        onKeyUp={handleKeyUp}
        onMouseUp={handleMouseUp}
        parentTaxonomyType={parentTaxonomyType}
        selected={selected}
        onClick={onSelected}
      >
        <div className="flex justify-between">
          <div className="flex flex-col gap-y-4 justify-center">
            <span className="font-bold">{title}</span>
            <span className="text-xs">{subtitle}</span>
          </div>
          <div className="flex flex-col gap-y-4 items-center">
            <span
              className={cx(
                'flex gap-x-1 items-center justify-center bg-gray-300 bg-opacity-40 rounded-2xl w-12 bg-opacity-25',
                { 'bg-opacity-25': selected }
              )}
            >
              <InboxOutlined
                className={cx('h-4', { 'text-white': selected })}
              />
              {count}
            </span>
            <RightOutlined />
          </div>
        </div>
      </TaxonomyItemContainer>
    </Dropdown>
  );
};

export default TaxonomyItem;
