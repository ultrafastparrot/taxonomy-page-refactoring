import {
  CopyOutlined,
  DeleteOutlined,
  EditOutlined,
  SwapOutlined,
} from '@ant-design/icons';
import React from 'react';
import { Action } from './TaxonomyItem';
import { TaxonomyType } from './TaxonomyCard';

export const getMenuActions = (taxonomyType: TaxonomyType): Action[] => {
  const commonActions = [
    {
      icon: <EditOutlined />,
      name: 'Editar',
      onClick: () => ({}),
    },
  ];

  const extendedActions =
    taxonomyType === 'BU'
      ? []
      : [
          {
            icon: <SwapOutlined />,
            name: 'Mover',
            onClick: () => ({}),
          },
          {
            icon: <CopyOutlined />,
            name: 'Duplicar',
            onClick: () => ({}),
          },
          {
            icon: <DeleteOutlined />,
            name: 'Excluir',
            onClick: () => ({}),
          },
        ];

  return [...commonActions, ...extendedActions];
};
