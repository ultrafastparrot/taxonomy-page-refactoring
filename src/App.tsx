import React from 'react';
import TaxonomyCard, { TaxonomyType } from './components/Taxonomy/TaxonomyCard';

const App = (): JSX.Element => {
  return (
    <div className="flex h-full justify-evenly gap-x-4 p-3">
      {[...Array(7).fill(0).keys()].map((index) => {
        let taxonomyType: TaxonomyType;

        if (index === 0) {
          taxonomyType = 'BU';
        } else if (index === 1) {
          taxonomyType = 'SEGMENT';
        } else {
          taxonomyType = 'OTHER';
        }

        return <TaxonomyCard taxonomyType={taxonomyType} key={index} />;
      })}
    </div>
  );
};

export default App;
