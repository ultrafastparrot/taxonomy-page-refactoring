import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import App from './App';

describe('App', () => {
  it('should pass', () => {
    const { queryByText } = render(<App />);
    expect(queryByText('Consegue me desbloquear?')).toBeInTheDocument();
  });
});
