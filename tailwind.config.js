module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        green: {
          DEFAULT: '#27ae60',
          light: '#55c51d',
          dark: '#378f0a',
        },
      },
    },
  },
  variants: {
    extend: {},
    borderWidth: ['first', 'last'],
  },
  plugins: [],
};
